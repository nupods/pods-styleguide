(function($) {

  /**
   * Masthead
   */
  // Add toggling
  $('.masthead__toggler').on('click touch', function(e) {
    $('body').toggleClass('menu-open');
  });

  // Reveal mobile submenus
  $('.menu-item-has-children > a')
    .on('click touch', function(e) {
      e.preventDefault();

      var $this = $(this),
          $parent = $this.parent(),
          $submenu = $parent.find('.sub-menu'),
          $siblings = $parent.siblings().find('.sub-menu');

      // Condition block if menu has been opened
      if ($parent.hasClass('sub-menu--open') || $(window).width() > 991) {
        window.location.href = $this.attr('href');
      } else {
        $parent.siblings().removeClass('sub-menu--open');
      }

      // Condition block if sibling is visible
      if ($siblings.is(':visible')) {
        $siblings.hide(350);
      }

      // Add .menu-open when clicked
      $parent.addClass('sub-menu--open');
      $submenu.show(350);
    });


  /**
   * Interior Navigation
   */
  $('.nav--interior__toggler').on('click touch', function(e) {
    $('body').toggleClass('nav--interior--open');
  });


  // load some images and randomize them
  var path = '//farm1.staticflickr.com/';
  var images = [
    '553/19758726802_2022a07725_z_d.jpg',
    '400/19577944200_6669cb62b3_z_d.jpg',
    '406/19579417179_e850c6a0f9_z_d.jpg',
    '3780/19758709242_01953f9f15_z_d.jpg',
    '3764/19758758392_3a0a9aa7b8_z_d.jpg',
    '3707/19766003205_e63672ccee_z_d.jpg',
    '525/19770700111_c0b5b2b469_z_d.jpg',
    '545/19758680802_29d7d281fd_z_d.jpg'
  ];

  function getRandImg() {
    return images[Math.floor(Math.random() * images.length)];
  }

  $('.pods-sg .card__image__wrapper').each(function() {
    $(this).append("<img class='card__image' src='" + path + getRandImg() + "' alt=''>");
  });

  $('.pods-sg .blockquote__image__wrapper').each(function() {
    $(this).append("<img class='blockquote__image' src='" + path + getRandImg() + "' alt=''>");
  });


  /* colors: pods-sg-swatch generation */

  // convert computed rgb to hex
  function rgb2hex(rgb){
   rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
   return (rgb && rgb.length === 4) ? "#" +
    ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
    ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
    ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
  }

  $('.pods-sg-swatch div').append(function() {
    var $this = $(this);
    var hex = rgb2hex($this.css('backgroundColor'));
    $this.data('hex',hex);
    return '<span class="hex-color">' + hex + '</span>';
  });

  $('.pods-sg-swatch div').each(function() {
    var $this = $(this);
    var hex = rgb2hex($this.css('backgroundColor'));
    $this.attr('data-hex', hex);
  });

  $('.pods-sg-swatch div').click(function() {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(this).data('hex')).select();
    document.execCommand("copy");
    $temp.remove();
  });

  $('.pods-sg-navpalette__item').click(function(e) {
    e.preventDefault();
    var palette = $(this).data('palette');
    var component = $(this).data('component');

    $('.pods-sg-navpalette__title a')
      .text(palette)
      .removeClass()
      .addClass('text--'+ palette +'-500');

    $('.'+ component).each(function() {
      var pattern = /(\w+)--(\w+)-(\w+)-|(\w+)--(\w+)-/;
      var color = component +'--'+ palette +'-';
      this.className = this.className.replace(pattern, color);
    });
  });

  /* Dynamically display responsive font sizes on typography page.
   * // * * Requires page reload after viewport resize. * * // *
   */
  var htmlFontSize = $("html").css("fontSize");
  $(".html-font-size").text(parseInt(htmlFontSize));

  $(".element-font-size").each(function() {
    var elemFontSize = $(this).css("fontSize");
    elemFontSize = Math.round(parseFloat(elemFontSize));
    $(this).text(elemFontSize + "px");
  });

})(jQuery);
