<?php $sg = (isset($_GET['item']) ? $_GET['item'] : null); ?>
<?php include 'functions.php'; ?>

<!doctype html>
<html>
  <?php include 'templates/_head.php'; ?>
  <body class="pods-sg<?= ' item--' . $sg; ?>">
    <?php
      include 'templates/_header.php';
    ?>

    <div class="wrap" role="document">
      <main class="main" id="main_content">
        <?php include 'templates/_index.php'; ?>
      </main>
    </div>

    <?php include 'templates/_footer.php'; ?>
  </body>
</html>
