<a class="skip alert alert--tertiary alert--sm ta--c tt--u" href="#main_content">Skip to main content <i class="icon ion-chevron-right"></i></a>
<header class="pods-sg-header">

  <?php if($sg) : ?>
    <a class="pods-sg-logo" href="/">
      nu<span style="color:#c00;">pods</span><span style="color:#999;">styleguide</span>
    </a>

    <nav class="pods-sg-nav">
      <a class="pods-sg-nav__title" href="#">
        <?= ($sg ? $sg : 'Components'); ?>
      </a>

      <div class="pods-sg-nav__list">
        <?php foreach (getStyleGuideFiles() as $file) : ?>
          <a class="pods-sg-nav__item <?= ($file == $_GET['item'] ? 'pods-sg-nav__item--active' : ''); ?>"
            href="/?item=<?= $file; ?> ">
            <?= $file; ?>
          </a>
        <?php endforeach; ?>
      </div>
    </nav>


  <?php endif; ?>

</header>
