<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PODS Styleguide</title>
  <link href='//fonts.googleapis.com/css?family=Lato:300,300italic,400,400italic,600,600italic,700,700italic,900' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="/dist/styles/styleguide.css">
</head>
