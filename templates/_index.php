<?php $sg = (isset($_GET['item']) ? $_GET['item'] : null); ?>

<?php if (!$sg) : ?>
  <div class="subtle-bg">
    <div class="container p--b-2@xs m--b-2@xs">
      <div class="pods-sg-page-subtitle ta--c p--y-2@xs">
        <h2 class="pods-sg-logo pods-sg-logo--home p--b-0@xs">nu<span style="color:#c00;">pods</span><span style="color:#999;">styleguide</span>
        <?= "<span class='fs--sm fw--300' style='letter-spacing: -1px;'>" . getBowerData() . "</span>"; ?></h2>
      </div>
      <div class="row">
        <?php foreach (getStyleGuideFiles() as $file) : ?>
          <div class="col-12@xs col-6@sm col-3@lg">
            <div class="card card--middle card--sg-home">
              <a class="card__block-link" href="/?item=<?= $file; ?>">
                <div class="card__inner">
                  <h4 class="card__title"><?= $file; ?></h4>
                </div>
              </a>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>

<?php else : ?>
  <?php include'templates/items/'.$sg.'.php'; ?>

<?php endif; ?>
