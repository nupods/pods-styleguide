<footer class="pods-sg-footer text-xs-center">
  <p class="p--y-3@xs">
    <a class="btn btn--outline btn--sm btn--gray-dark" href="#">Back to Top</a>
  </p>
  <p class="p--y-2@xs">
    <a class="pods-sg-logo" href="/styleguide">nupodsstyleguide</a>
  </p>
</footer>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="/dist/scripts/main.js"></script>
<script src="/dist/scripts/styleguide.js"></script>

