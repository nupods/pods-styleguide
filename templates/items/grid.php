<section class="pods-sg-section pods-show-grid">
  <div class="container">
    <div class="row">
      <div class="col-12@xs col-3@sm col-4@md col-1@lg"><p>1</p></div>
      <div class="col-12@xs col-3@sm col-4@md col-1@lg"><p>1</p></div>
      <div class="col-12@xs col-3@sm col-4@md col-1@lg"><p>1</p></div>
      <div class="col-12@xs col-3@sm col-4@md col-1@lg"><p>1</p></div>
      <div class="col-12@xs col-3@sm col-4@md col-1@lg"><p>1</p></div>
      <div class="col-12@xs col-3@sm col-4@md col-1@lg"><p>1</p></div>
      <div class="col-12@xs col-3@sm col-4@md col-1@lg"><p>1</p></div>
      <div class="col-12@xs col-3@sm col-4@md col-1@lg"><p>1</p></div>
      <div class="col-12@xs col-3@sm col-4@md col-1@lg"><p>1</p></div>
      <div class="col-12@xs col-3@sm col-4@md col-1@lg"><p>1</p></div>
      <div class="col-12@xs col-3@sm col-4@md col-1@lg"><p>1</p></div>
      <div class="col-12@xs col-3@sm col-4@md col-1@lg"><p>1</p></div>
    </div>
    <div class="row">
      <div class="col-12@xs col-6@sm col-4@md col-2@lg"><p>2</p></div>
      <div class="col-12@xs col-6@sm col-4@md col-2@lg"><p>2</p></div>
      <div class="col-12@xs col-6@sm col-4@md col-2@lg"><p>2</p></div>
      <div class="col-12@xs col-6@sm col-4@md col-2@lg"><p>2</p></div>
      <div class="col-12@xs col-6@sm col-4@md col-2@lg"><p>2</p></div>
      <div class="col-12@xs col-6@sm col-4@md col-2@lg"><p>2</p></div>
    </div>
    <div class="row">
      <div class="col-12@xs col-3@md"><p>3</p></div>
      <div class="col-12@xs col-3@md"><p>3</p></div>
      <div class="col-12@xs col-3@md"><p>3</p></div>
      <div class="col-12@xs col-3@md"><p>3</p></div>
    </div>
    <div class="row">
      <div class="col-12@xs col-4@md"><p>4</p></div>
      <div class="col-12@xs col-4@md"><p>4</p></div>
      <div class="col-12@xs col-4@md"><p>4</p></div>
    </div>
    <div class="row">
      <div class="col-12@xs col-5@md"><p>5</p></div>
      <div class="col-12@xs col-7@md"><p>7</p></div>
    </div>
    <div class="row">
      <div class="col-12@xs col-6@md"><p>6</p></div>
      <div class="col-12@xs col-6@md"><p>6</p></div>
    </div>
    <div class="row">
      <div class="col-12@xs col-7@md"><p>7</p></div>
      <div class="col-12@xs col-5@md"><p>5</p></div>
    </div>
    <div class="row">
      <div class="col-12@xs col-8@md"><p>8</p></div>
      <div class="col-12@xs col-4@md"><p>4</p></div>
    </div>
    <div class="row">
      <div class="col-12@xs col-9@md"><p>9</p></div>
      <div class="col-12@xs col-3@md"><p>3</p></div>
    </div>
    <div class="row">
      <div class="col-12@xs col-10@md"><p>10</p></div>
      <div class="col-12@xs col-2@md"><p>2</p></div>
    </div>
    <div class="row">
      <div class="col-12@xs col-11@md"><p>11</p></div>
      <div class="col-12@xs col-1@md"><p>1</p></div>
    </div>
    <div class="row">
      <div class="col-12@xs"><p>12</p></div>
    </div>
  </div>
</section>
