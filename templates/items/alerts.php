<section class="pods-sg-section">
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-12@xs">
        <div class="alert alert--xs">
          <b>Alert XS</b>: Lorem ipsum dolor sit amet defaults to <code>.inline-block</code>, consectetur adipisicing elit. Ut enim ad minim veniam.
        </div>
        <div class="alert alert--sm">
          <b>Alert SM</b>: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <a href="#">Inline link example</a>
        </div>
        <div class="alert alert--teal">
            <div class="alert__heading">Alert Base</div>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. <a href="#" class="alert__link">Learn More</a>
        </div>
        <div class="alert alert--lg alert--teal alert--outline">
          <b>Alert LG</b>: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
        </div>
      </div>
    </div>
  </div>
</section>
<section class="pods-sg-section dark-bg">
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-12@xs">
        <div class="alert alert--xs">
          <b>Alert XS</b>: Lorem ipsum dolor sit amet defaults to <code>.inline-block</code>, consectetur adipisicing elit. Ut enim ad minim veniam.
        </div>
        <div class="alert alert--sm">
          <b>Alert SM</b>: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <a href="#">Inline link example</a>
        </div>
        <div class="alert alert--green-light">
            <div class="alert__heading">Alert Base</div>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. <a href="#" class="alert__link"><i class="ion ion-android-arrow-forward"></i> Read More</a>
        </div>
        <div class="alert alert--lg alert--green-light alert--outline">
          <b>Alert LG</b>: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
        </div>
      </div>
    </div>
  </div>
</section>
