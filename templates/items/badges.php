<section class="pods-sg-section">
  <div class="container p--y-2@xs">
    <div class="row">
      <?php $i = 1; while ($i < 4) : ?>
        <div class="col-12@xs col-4@md">
          <div class="badge__wrapper subtle-bg">
            <div
              data-component="badge"
              data-component-color="red"
              class="badge">
                <a href="#">Badge (Link)</a>
                <i class="badge__icon ion-ios-videocam"></i>
            </div>
          </div>
        </div>
      <?php $i++; endwhile; ?>
      <?php $i = 4; while ($i < 7) : ?>
        <div class="col-12@xs col-4@md">
          <div class="badge__wrapper subtle-bg">
            <div
              data-component="badge"
              data-component-color="red"
              class="badge badge--center">
              Badge Center
            </div>
          </div>
        </div>
      <?php $i++; endwhile; ?>
      <?php $i = 7; while ($i < 10) : ?>
        <div class="col-12@xs col-4@md">
          <div class="badge__wrapper subtle-bg">
            <div
              data-component="badge"
              data-component-color="red"
              class="badge badge--right badge--pill">
              Badge Right (Pill)
            </div>
          </div>
        </div>
      <?php $i++; endwhile; ?>
    </div>
  </div>
</section>
