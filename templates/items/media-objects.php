<section class="pods-sg-section">
  <header class="pods-sg-section__header">Base</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-12@xs col-8@md offset-2@md col-6@lg offset-3@lg">
        <div class="media media--xs">
          <a class="media__block-link" href="#">
            <div class="media__image__wrapper">
              <img class="media__image" src="http://www.josephklevenefineartltd.com/AndyWarholZebra.jpg" alt="">
            </div>
            <div class="media__body">
              <h4 class="media__title">This is a XS media object.</h4>
              <p class="media__copy">"C" is for "cookie", nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur.</p>
            </div>
          </a>
        </div>
        <div class="media media--sm">
          <a class="media__block-link" href="#">
            <div class="media__image__wrapper">
              <img class="media__image" src="https://learnodo-newtonic.com/wp-content/uploads/2013/07/Marilyn-Diptych-Close-Up.jpg" alt="">
            </div>
            <div class="media__body">
              <h4 class="media__title">This is a SM media object.</h4>
              <p class="media__copy">"C" is for "cookie", nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur.</p>
            </div>
          </a>
        </div>
        <div class="media">
          <a class="media__block-link" href="#">
            <div class="media__image__wrapper">
              <img class="media__image" src="https://learnodo-newtonic.com/wp-content/uploads/2014/09/Mao-1973-Andy-Warhol.jpg" alt="">
            </div>
            <div class="media__body">
              <h4 class="media__title">This is a basic media object.</h4>
              <p class="media__copy">"C" is for "cookie", nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur.</p>
            </div>
          </a>
        </div>
        <div class="media media--lg">
          <a class="media__block-link" href="#">
            <div class="media__image__wrapper">
              <img class="media__image" src="https://theartjunkie.files.wordpress.com/2012/08/soup3.jpg?w=1112" alt="">
            </div>
            <div class="media__body">
              <h4 class="media__title">This is a LG media object.</h4>
              <p class="media__copy">"C" is for "cookie", nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur.</p>
            </div>
          </a>
        </div>
        <div class="media">
          <a class="media__block-link" href="#">
            <div class="media__image__wrapper">
              <img class="media__image" src="/dist/images/16-9-media-object-image.jpg" alt="">
            </div>
            <div class="media__body">
              <h4 class="media__title">This basic media object contains a 16:9 image.</h4>
              <p class="media__copy">"C" is for "cookie", nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur.</p>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

  <!-- media object list -->
<section class="pods-sg-section">
  <header class="pods-sg-section__header">Media Object List (Default: circular thumb)</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-12@xs col-8@md offset-2@md col-6@lg offset-3@lg">
        <ul class="media__list">
          <li class="media">
            <a class="media__block-link" href="#">
              <div class="media__image__wrapper">
                <img class="media__image" src="http://content.wfmynews2.com/photo/2016/11/01/Cookie%20Monster%20USAT_1478046170218_6692962_ver1.0.JPG" alt="">
              </div>
              <div class="media__body">
                <h4 class="media__title">This: media object list item.</h4>
              <p class="media__copy">"C" is for "cookie", nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Nomnomnom...</p>
              </div>
            </a>
          </li>
          <li class="media">
            <a class="media__block-link" href="#">
              <div class="media__image__wrapper">
                <img class="media__image" src="https://s-media-cache-ak0.pinimg.com/236x/c3/ca/67/c3ca67481cd8019f8aa863d83300f2a5.jpg" alt="">
              </div>
              <div class="media__body">
                <h4 class="media__title">Another media object list item.</h4>
                <p class="media__copy">Animal etiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit amet fermentum eat drums!</p>
              </div>
            </a>
          </li>
          <li class="media">
            <a class="media__block-link" href="#">
              <div class="media__image__wrapper">
                <img class="media__image" src="//vignette3.wikia.nocookie.net/muppet/images/3/3c/CT-p0001-ST.jpg/revision/latest?cb=20060205225316" alt="">
              </div>
              <div class="media__body">
                <h4 class="media__title">Three media object items! &mdash; ah-ah-aahh!</h4>
                <p class="media__copy">One praesent commodo cursus magna, two vel scelerisque nisl consectetur et, three maecenas sed diam eget risus varius blandit sit amet non magna, ah-ah-aahh!</p>
              </div>
            </a>
          </li>
          <li class="media">
            <a class="media__block-link" href="#">
              <div class="media__image__wrapper">
                <img class="media__image" src="https://muppetmindset.files.wordpress.com/2009/11/849d4-oscarandslimey.jpg" alt="">
              </div>
              <div class="media__body">
                <h4 class="media__title">I love trash.</h4>
                <p class="media__copy">Smelly morbi leo risus, porta ac consectetur ac stink, vestibulum at eros. Moldy aenean lacinia bibendum nulla sed consectetur.</p>
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>

  <!-- media object list -->
<section class="pods-sg-section">
  <header class="pods-sg-section__header">Media Object List (Variation: rectangular thumb)</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-12@xs col-8@md offset-2@md col-6@lg offset-3@lg">
        <ul class="media__list media__list--rect">
          <li class="media">
            <a class="media__block-link" href="#">
              <div class="media__image__wrapper">
                <img class="media__image" src="http://content.wfmynews2.com/photo/2016/11/01/Cookie%20Monster%20USAT_1478046170218_6692962_ver1.0.JPG" alt="">
              </div>
              <div class="media__body">
                <h4 class="media__title">This: media object list item.</h4>
              <p class="media__copy">"C" is for "cookie", nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Nomnomnom...</p>
              </div>
            </a>
          </li>
          <li class="media">
            <a class="media__block-link" href="#">
              <div class="media__image__wrapper">
                <img class="media__image" src="https://s-media-cache-ak0.pinimg.com/236x/c3/ca/67/c3ca67481cd8019f8aa863d83300f2a5.jpg" alt="">
              </div>
              <div class="media__body">
                <h4 class="media__title">Another media object list item.</h4>
                <p class="media__copy">Animal etiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit amet fermentum eat drums!</p>
              </div>
            </a>
          </li>
          <li class="media">
            <a class="media__block-link" href="#">
              <div class="media__image__wrapper">
                <img class="media__image" src="//vignette3.wikia.nocookie.net/muppet/images/3/3c/CT-p0001-ST.jpg/revision/latest?cb=20060205225316" alt="">
              </div>
              <div class="media__body">
                <h4 class="media__title">Three media object items! &mdash; ah-ah-aahh!</h4>
                <p class="media__copy">One praesent commodo cursus magna, two vel scelerisque nisl consectetur et, three maecenas sed diam eget risus varius blandit sit amet non magna, ah-ah-aahh!</p>
              </div>
            </a>
          </li>
          <li class="media">
            <a class="media__block-link" href="#">
              <div class="media__image__wrapper">
                <img class="media__image" src="https://muppetmindset.files.wordpress.com/2009/11/849d4-oscarandslimey.jpg" alt="">
              </div>
              <div class="media__body">
                <h4 class="media__title">I love trash.</h4>
                <p class="media__copy">Smelly morbi leo risus, porta ac consectetur ac stink, vestibulum at eros. Moldy aenean lacinia bibendum nulla sed consectetur.</p>
              </div>
            </a>
          </li>
          <li class="media">
            <a class="media__block-link" href="#">
              <div class="media__image__wrapper">
                <img class="media__image" src="/dist/images/16-9-media-object-image.jpg" alt="">
              </div>
              <div class="media__body">
                <h4 class="media__title">Showing a 16:9 image.</h4>
                <p class="media__copy">Sixtini tui ninus ratiati immajis, porta ac consectetur ac stink, vestibulum at eros. Moldy aenean lacinia bibendum nulla sed consectetur.</p>
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
