<section class="pods-sg-section">
  <header class="pods-sg-section__header">Base</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-3@md">
        <div class="list-group list-group--xs">
          <div class="list-group__item">Lorem ipsum dolor sit amet do eiusmod</div>
          <div class="list-group__item">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
          <div class="list-group__item">Maecenas faucibus mollis interdum</div>
          <div class="list-group__item"><a href="#">Integer posuere erat a ante dapibus posuere aliquet</a></div>
          <div class="list-group__item"><a href="#">Nullam id dolor id nibh ultricies vehicula ut id elit</a></div>
          <div class="list-group__item"><a href="#">Nullam id dolor id nibh ultricies vehicula ut id elit</a></div>
        </div>
      </div>
      <div class="col-4@md">
        <div class="list-group list-group--sm">
          <div class="list-group__item">Lorem ipsum dolor sit amet do eiusmod</div>
          <div class="list-group__item">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
          <div class="list-group__item">Maecenas faucibus mollis interdum</div>
          <div class="list-group__item"><a href="#">Integer posuere erat a ante dapibus posuere aliquet</a></div>
          <div class="list-group__item"><a href="#">Nullam id dolor id nibh ultricies vehicula ut id elit</a></div>
          <div class="list-group__item"><a href="#">Nullam id dolor id nibh ultricies vehicula ut id elit</a></div>
        </div>
      </div>
      <div class="col-5@md">
        <div class="list-group">
          <div class="list-group__item">Lorem ipsum dolor sit amet do eiusmod</div>
          <div class="list-group__item">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
          <div class="list-group__item">Maecenas faucibus mollis interdum</div>
          <a href="#" class="list-group__item">Integer posuere erat a ante dapibus posuere aliquet</a>
          <a href="#" class="list-group__item">Nullam id dolor id nibh ultricies vehicula ut id elit</a>
          <a href="#" class="list-group__item">Nullam id dolor id nibh ultricies vehicula ut id elit</a>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="pods-sg-section dark-bg">
  <header class="pods-sg-section__header">White On Dark</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-3@md">
        <div class="list-group list-group--white list-group--xs">
          <div class="list-group__item">Lorem ipsum dolor sit amet do eiusmod</div>
          <div class="list-group__item">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
          <div class="list-group__item">Maecenas faucibus mollis interdum</div>
          <div class="list-group__item"><a href="#">Integer posuere erat a ante dapibus posuere aliquet</a></div>
          <div class="list-group__item"><a href="#">Nullam id dolor id nibh ultricies vehicula ut id elit</a></div>
          <div class="list-group__item"><a href="#">Nullam id dolor id nibh ultricies vehicula ut id elit</a></div>
        </div>
      </div>
      <div class="col-4@md">
        <div class="list-group list-group--white list-group--sm">
          <div class="list-group__item">Lorem ipsum dolor sit amet do eiusmod</div>
          <div class="list-group__item">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
          <div class="list-group__item">Maecenas faucibus mollis interdum</div>
          <div class="list-group__item"><a href="#">Integer posuere erat a ante dapibus posuere aliquet</a></div>
          <div class="list-group__item"><a href="#">Nullam id dolor id nibh ultricies vehicula ut id elit</a></div>
          <div class="list-group__item"><a href="#">Nullam id dolor id nibh ultricies vehicula ut id elit</a></div>
        </div>
      </div>
      <div class="col-5@md">
        <div class="list-group list-group--white">
          <div class="list-group__item">Lorem ipsum dolor sit amet do eiusmod</div>
          <div class="list-group__item">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
          <div class="list-group__item">Maecenas faucibus mollis interdum</div>
          <a href="#" class="list-group__item">Integer posuere erat a ante dapibus posuere aliquet</a>
          <a href="#" class="list-group__item">Nullam id dolor id nibh ultricies vehicula ut id elit</a>
          <a href="#" class="list-group__item">Nullam id dolor id nibh ultricies vehicula ut id elit</a>
        </div>
      </div>
    </div>
  </div>
</section>
