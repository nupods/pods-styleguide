<section class="pods-sg-section">
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-12@xs">
        <div class="accordion" id="pods_accordion_light">
          <div class="card">
            <div class="accordion__trigger" data-toggle="collapse" data-parent="#pods_accordion_light" data-target="#collapse1" aria-expanded="true" aria-controls="collapseExampleLight">Play me some accordion!</div>
            <div class="accordion__content collapse in" id="collapse1">
              <div class="accordion__inner">
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                <p>Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="accordion__trigger collapsed" data-toggle="collapse" data-parent="#pods_accordion_light" data-target="#collapse2" aria-expanded="false" aria-controls="collapseExampleLight">Do you want to play some polka?</div>
            <div class="accordion__content collapse" id="collapse2">
              <div class="accordion__inner">
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                <p>Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="accordion__trigger collapsed" data-toggle="collapse" data-parent="#pods_accordion_light" data-target="#collapse3" aria-expanded="false" aria-controls="collapseExampleLight">Roll out the barrel!</div>
            <div class="accordion__content collapse" id="collapse3">
              <div class="accordion__inner">
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                <p>Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="pods-sg-section dark-bg">
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-12@xs">
        <div class="accordion" id="pods_accordion_dark">
          <div class="card">
            <div class="accordion__trigger" data-toggle="collapse" data-parent="#pods_accordion_dark" data-target="#collapse4" aria-expanded="false" aria-controls="collapseExampleDark">Play me some accordion!</div>
            <div class="accordion__content collapse in" id="collapse4">
              <div class="accordion__inner">
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                <p>Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="accordion__trigger collapsed" data-toggle="collapse" data-parent="#pods_accordion_dark" data-target="#collapse5" aria-expanded="false" aria-controls="collapseExampleDark">Do you want to play some polka?</div>
            <div class="accordion__content collapse" id="collapse5">
              <div class="accordion__inner">
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                <p>Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="accordion__trigger collapsed" data-toggle="collapse" data-parent="#pods_accordion_dark" data-target="#collapse6" aria-expanded="false" aria-controls="collapseExampleDark">Roll out the barrel!</div>
            <div class="accordion__content collapse" id="collapse6">
              <div class="accordion__inner">
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                <p>Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
