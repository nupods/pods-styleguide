<section class="pods-sg-section">
  <header class="pods-sg-section__header">Basic Table</header>
  <div class="container p--y-2@xs cards">
    <div class="row">
      <div class="col-12@xs">
        <table class="table">
          <thead>
            <tr class="th--sm">
              <th width="20%">Dolor Vestibulum</th>
              <th width="40%">Commodo Vehicula</th>
              <th width="40%">Elit Mattis Mollis</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Ultricies Justo</th>
              <td>Aenean lacinia bibendum nulla sed consectetur.</td>
              <td>Cras mattis consectetur purus sit amet fermentum.</td>
            </tr>
            <tr>
              <th>Lorem Nullam</th>
              <td>Donec ullamcorper nulla non metus auctor fringilla.</td>
              <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
            </tr>
            <tr>
              <th>Adipiscing Condimentum</th>
              <td>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</td>
              <td>Curabitur blandit tempus porttitor.</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>

<section class="pods-sg-section">
  <header class="pods-sg-section__header">With Vertical Borders</header>
  <div class="container p--y-2@xs cards">
    <div class="row">
      <div class="col-12@xs">
        <table class="table table--border-vertical">
          <thead>
            <tr class="th--sm">
              <th width="20%">Dolor Vestibulum</th>
              <th width="40%">Commodo Vehicula</th>
              <th width="40%">Elit Mattis Mollis</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Ultricies Justo</th>
              <td>Aenean lacinia bibendum nulla sed consectetur.</td>
              <td>Cras mattis consectetur purus sit amet fermentum.</td>
            </tr>
            <tr>
              <th>Lorem Nullam</th>
              <td>Donec ullamcorper nulla non metus auctor fringilla.</td>
              <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
            </tr>
            <tr>
              <th>Adipiscing Condimentum</th>
              <td>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</td>
              <td>Curabitur blandit tempus porttitor.</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>

<section class="pods-sg-section">
  <header class="pods-sg-section__header">With Alternating Stripes</header>
  <div class="container p--y-2@xs cards">
    <div class="row">
      <div class="col-12@xs">
        <table class="table table--striped">
          <thead>
            <tr class="th--sm">
              <th width="20%">Dolor Vestibulum</th>
              <th width="40%">Commodo Vehicula</th>
              <th width="40%">Elit Mattis Mollis</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Ultricies Justo</th>
              <td>Aenean lacinia bibendum nulla sed consectetur.</td>
              <td>Cras mattis consectetur purus sit amet fermentum.</td>
            </tr>
            <tr>
              <th>Lorem Nullam</th>
              <td>Donec ullamcorper nulla non metus auctor fringilla.</td>
              <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
            </tr>
            <tr>
              <th>Fusce Condimentum</th>
              <td>Adipiscing dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</td>
              <td>Curabitur blandit tempus porttitor.</td>
            </tr>
            <tr>
              <th>Justo Ultricies</th>
              <td>Aenean lacinia bibendum nulla sed consectetur.</td>
              <td>Cras mattis consectetur purus sit amet fermentum.</td>
            </tr>
            <tr>
              <th>Accio Lorem Ipsum</th>
              <td>Donec ullamcorper nulla non metus auctor fringilla.</td>
              <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
            </tr>
            <tr>
              <th>Adipiscing Condimentum</th>
              <td>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</td>
              <td>Curabitur blandit tempus porttitor.</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>

<section class="pods-sg-section">
  <header class="pods-sg-section__header">Responsive (Vertically-Scrolling) Table</header>
  <div class="container p--y-2@xs cards">
    <div class="row">
      <div class="col-12@xs">
        <table class="table table--responsive">
          <thead>
            <tr class="th--sm">
              <th width="25%">Dolor Vestibulum</th>
              <th width="15%">Commodo Vehicula</th>
              <th width="15%">Elit Mattis Mollis</th>
              <th width="15%">Dolor Vestibulum</th>
              <th width="15%">Commodo Vehicula</th>
              <th width="15%">Elit Mattis Mollis</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Ultricies Justo</th>
              <td>Aenean lacinia bibendum nulla sed consectetur.</td>
              <td>Cras mattis consectetur purus sit amet fermentum.</td>
              <td>Lorem Nullam</td>
              <td>Donec ullamcorper nulla non metus auctor fringilla.</td>
              <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
            </tr>
            <tr>
              <th>Fusce Condimentum</th>
              <td>Adipiscing dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</td>
              <td class="shaded">Shading on this cell (using <code>td.shaded</code>). Curabitur blandit tempus porttitor.</td>
              <td>Justo Ultricies</td>
              <td>Aenean lacinia bibendum nulla sed consectetur.</td>
              <td>Cras mattis consectetur purus sit amet fermentum.</td>
            </tr>
            <tr>
              <th>Accio Lorem Ipsum</th>
              <td>Donec ullamcorper nulla non metus auctor fringilla.</td>
              <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
              <td>Adipiscing Condimentum</td>
              <td class="shaded">Shading on this cell (using <code>td.shaded</code>). Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</td>
              <td>Curabitur blandit tempus porttitor.</td>
            </tr>
            <tr>
              <th>Ultricies Justo</th>
              <td>Aenean lacinia bibendum nulla sed consectetur.</td>
              <td>Cras mattis consectetur purus sit amet fermentum.</td>
              <td>Lorem Nullam</td>
              <td>Donec ullamcorper nulla non metus auctor fringilla.</td>
              <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
            </tr>
            <tr class="shaded">
              <th>Shading On Whole Row (using <code>tr.shaded</code>)</th>
              <td>Adipiscing dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</td>
              <td>Shading on this cell. Curabitur blandit tempus porttitor.</td>
              <td>Justo Ultricies</td>
              <td>Aenean lacinia bibendum nulla sed consectetur.</td>
              <td>Cras mattis consectetur purus sit amet fermentum.</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>