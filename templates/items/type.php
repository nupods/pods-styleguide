<section>
  <div class="banner banner--image banner--lg banner--middle">
    <div class="container">
      <div class="row">
        <div class="col-12@xs">
          <div class="banner__pretitle">What's your type?</div>
          <h1 class="banner__title">Typography Styles</h1>
          <div class="banner__subtitle">This is a page of base typography styles for the Undergraduate Education parent theme.</div>
        </div>
      </div>
    </div>
  </div>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-12@xs col-10@lg offset-1@lg">
        <h1>Here is Heading 1 <span class="show-font-size">(<span class="element-font-size"></span> @ 2.25rem)</span></h1>
        <p class="lead"><b>This is a lead paragraph</b> <span class="show-font-size">(<span class="element-font-size"></span> @ 1.25rem)</span>. The pixel values currently shown on this page are rounded to the nearest whole-number in pixels from a rem value times the <span class="text--highlight"><span class="html-font-size"></span>-pixel html base</span> as read from the last page load (<em>note that, for now, you'll need to refresh the page if you resize the viewport and want to see the updated pixel values for that viewport width</em>).</p>
        <ul class="list--unstyled fs--sm">
          <li>15px base font-size (<code>xs</code>)</li>
          <li>16px base font-size (<code>sm</code>, <code>md</code>, and <code>lg</code>)</li>
          <li>17px base font-size (<code>xl</code>)</li>
          <li>18px base font-size (<code>xx</code>)</li>
        </ul>
        <p>Body copy <span class="show-font-size">(<span class="element-font-size"></span> @ 1rem)</span> Cras mattis consectetur purus sit amet fermentum. Donec id elit non mi porta gravida at eget metus. Donec ullamcorper nulla non metus auctor fringilla. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula ut id elit. Nullam id dolor id nibh ultricies vehicula ut id elit. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
        <p class="fs--sm">This a little note set at <code>font-size-sm</code> <span class="show-font-size">(<span class="element-font-size"></span> @ 0.875rem)</span>.</p>
        <h2>Here is a Heading Two <span class="show-font-size">(<span class="element-font-size"></span> @ 2rem)</span></h2>
        <p>Vestibulum id ligula porta felis euismod semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
        <h3>Heading Three and Listy Lists <span class="show-font-size">(<span class="element-font-size"></span> @ 1.5rem)</span></h3>
        <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Donec sed odio dui. Aenean lacinia bibendum nulla sed consectetur. Maecenas sed diam eget risus varius blandit sit amet non magna. Aenean lacinia bibendum nulla sed consectetur.</p>
        <h4>Default List Style</h4>
        <ul>
          <li>Color &mdash; <i>Nullam quis risus eget urna mollis ornare vel eu leo.</i></li>
          <li>Typography &mdash; <i>Aenean lacinia bibendum nulla sed consectetur.</i>
            <ul>
              <li>Font family &mdash; <i>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</i></li>
              <li>Font style &mdash; <i>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nullam quis risus eget urna mollis ornare vel.</i>
                <ul>
                  <li>Normal &mdash; <i>Nullam quis risus eget urna mollis ornare vel eu leo.</i></li>
                  <li><i>Italic</i> &mdash; <i>Aenean lacinia bibendum nulla sed consectetur.</i></li>
                  <li><b>Bold</b> &mdash; <i>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</i></li>
                  <li><b><i>Bold Italic</i></b> &mdash; <i>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</i></li>
                </ul>
              </li>
              <li>Line-height (leading) &mdash; <i>Nullam quis risus eget urna mollis ornare vel eu leo.</i></li>
              <li>Letter-spacing (tracking) &mdash; <i>Aenean lacinia bibendum nulla sed consectetur.</i></li>
            </ul>
          </li>
          <li>Imagery &mdash; <i>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</i>
            <ul>
              <li>Photography &mdash; <i>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</i></li>
              <li>Illustration &mdash; <i>Nullam quis risus eget urna mollis ornare vel eu leo.</i></li>
              <li>Scalable Vector Graphics <abbr>SVG</abbr> &mdash; <i>Aenean lacinia bibendum nulla sed consectetur.</i></li>
            </ul>
          </li>
        </ul>
        <p>Here's some trailing text for showing some flow. Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Sed posuere consectetur est at lobortis. Vestibulum id ligula porta felis euismod semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla.</p>

        <h4>Unstyled List Style</h4>
        <ul class="list--unstyled">
          <li>4 loaves whole wheat bread</li>
          <li>1 lb. SweetLeaf Farms unsalted butter</li>
          <li>1 bag unrefined sugar</li>
          <li>2 boxes graham crackers, crumbled</li>
          <li>1 bag butterscotch hard candies. <i>Now, this line item may require some explanation, at least for text-wrap demonstration: Vestibulum id ligula porta felis euismod semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla.</i>
            <ul class="list--unstyled">
              <li>Best not use Werther's. Those aren't butterscotch, they're caramel.</li>
              <li>Look for the ones individually wrapped in crinkly, transparent yellow cellophane.</li>
            </ul>
          </li>
          <li>17 rolls of tin foil</li>
          <li>9 sq feet of polysterene floor liner</li>
          <li>1 player piano</li>
        </ul>
        <p>Here's some trailing text for showing some flow. Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Sed posuere consectetur est at lobortis. Vestibulum id ligula porta felis euismod semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla.</p>

        <h4>Inline Lists</h4>
        <ul class="list--inline">
          <li><a href="#">item one</a></li>
          <li><a href="#">item two</a></li>
          <li><a href="#">item three</a></li>
          <li><a href="#">item four</a></li>
        </ul>
        <p>Here's some trailing text for showing some flow. Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Sed posuere consectetur est at lobortis. Vestibulum id ligula porta felis euismod semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla.</p>

        <h4>Ordered List</h4>
        <ol>
          <li>Place laptop onto kitchen table.</li>
          <li>Open laptop to its normal usage position.</li>
          <li>Walk over to your refrigerator.</li>
            <ol type="i">
              <li>Open the refrigerator door.</li>
              <li>Retrieve a carton of milk.
                <ol type="a">
                  <li>You may use whole, low-fat or skim.</li>
                  <li>Dairy milk will work best, but soy milk or almond milk can work, too.</li>
                  <li>If you have no milk, you can use juice or water.</li>
                </ol>
              </li>
              <li>Close the refrigerator door.</li>
            </ol>
          </li>
          <li>Return to table with milk.</li>
          <li>Open the container of milk.</li>
          <li>While holding the container above the computer, turn it over, pouring all the contents of the carton onto the laptop keyboard and screen.</li>
          <li>Make sure your files were backed up.</li>
        </ol>
        <p>Here's some trailing text for showing some flow. Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Sed posuere consectetur est at lobortis. Vestibulum id ligula porta felis euismod semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla.</p>

        <h4>Definition List</h4>
        <dl class="row">
          <dt class="col-3@xs">Single room</dt>
          <dd class="col-9@xs">$199/night <small>(breakfast included, <abbr>VAT</abbr> not included)</small></dd>

          <dt class="col-3@xs">Double room</dt>
          <dd class="col-9@xs">$239/night <small>(breakfast included, <abbr>VAT</abbr> not included)</small></dd>
        </dl>
        <p>Here's some trailing text for showing some flow. Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Sed posuere consectetur est at lobortis. Vestibulum id ligula porta felis euismod semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla.</p>

        <h4>May the Fourth be With You <span class="show-font-size">(<span class="element-font-size"></span> @ 1.25rem)</span></h4>
        <p>Sed posuere consectetur est at lobortis. Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
        <h5>Sometimes We'll Use Heading Five <span class="show-font-size">(<span class="element-font-size"></span> @ 1rem)</span></h5>
        <p>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cras mattis consectetur purus sit amet fermentum. Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
        <h6 class="text-uppercase">And Even Heading Six if We're Really Feeling It <span class="show-font-size">(<span class="element-font-size"></span> @ 1rem)</span></h6>
        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Nulla vitae elit libero, a pharetra augue. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas sed diam eget risus varius blandit sit amet non magna. Aenean lacinia bibendum nulla sed consectetur.</p>
        <p class="fs--xs">Here's a tiny footnote set at <code>font-size-xs</code> <span class="show-font-size">(<span class="element-font-size"></span> @ 0.75rem)</span>.</p>
        <hr>
        <p class="ds--1">Display Size 1: Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. <span class="show-font-size">(<span class="element-font-size"></span>)</p>
        <p class="ds--2">Display Size 2: Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. <span class="show-font-size">(<span class="element-font-size"></span>)</p>
        <p class="ds--3">Display Size 3: Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. <span class="show-font-size">(<span class="element-font-size"></span>)</p>
        <p class="ds--4">Display Size 4: Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. <span class="show-font-size">(<span class="element-font-size"></span>)</p>
        <p class="ds--5">Display Size 5: Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. <span class="show-font-size">(<span class="element-font-size"></span>)</p>
        <p class="ds--6">Display Size 6: Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. <span class="show-font-size">(<span class="element-font-size"></span>)</p>
      </div>
    </div>
  </section>
