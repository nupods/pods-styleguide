<section class="pods-sg-section subtle-bg">
  <header class="pods-sg-section__header">Base</header>
  <div class="container p--y-2@xs cards">
    <div class="row">
      <div class="col-12@xs col-6@sm col-3@lg">
        <div class="card">
          <a class="card__block-link" href="#">
            <h4 class="card__title">Basic Card</h4>
            <p class="card__text">This is the card in its most basic form.</p>
          </a>
        </div>
      </div>
      <div class="col-12@xs col-6@sm col-3@lg">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__inner">
              <h4 class="card__title">Basic Card with Inner Block</h4>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12@xs col-6@sm col-3@lg">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Basic Card with Image and Inner Block</h4>
              <p class="card__text">This card has an edge-to-edge image and text in an inner block. Adding the inner block gives padding to the card content.</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12@xs col-6@sm col-3@lg">
        <div class="card card--middle">
          <a class="card__block-link" href="#">
            <div class="card__inner">
              <h4 class="card__title">Basic Card, Centered x&nbsp;&amp;&nbsp;y</h4>
              <p class="card__text">This card's content is centered vertically and horizontally.</p>
            </div>
          </a>
        </div>
      </div>

      <!-- with color modifiers -->
      <div class="col-12@xs col-6@sm col-3@lg">
        <div class="card card--transparent">
          <a class="card__block-link" href="#">
            <h4 class="card__title">Basic Card</h4>
            <p class="card__text">Here is a basic card with the background set to transparent. Let your background shine through.</p>
          </a>
        </div>
      </div>

      <div class="col-12@xs col-6@sm col-3@lg">
        <div class="card card--teal">
          <a class="card__block-link" href="#">
            <header class="card__header">Card Header</header>
            <div class="card__inner">
              <h4 class="card__title">Basic Card, Primary</h4>
              <p class="card__text">This is a padded basic card with the background as the defined primary brand color.</p>
            </div>
            <div class="card__footer">
              <div class="ta--c text--uppercase">Learn More</div>
            </div>
          </a>
        </div>
      </div>

      <div class="col-12@xs col-3@lg">
        <div class="card card--scroll">
          <header class="card__header">Card Scroll</header>
          <div class="card__inner">
            <ul class="media__list">
              <li class="media media--xs media--right">
                <a class="media__block-link" href="/explore/event/nu-horizons-undergraduate-research-panel/">
                  <div class="media__text">
                    <h4 class="media__title">
                      <b>NU Horizons: Undergraduate Research Panel</b>
                      <time style="display: block; padding-top: .25rem;" class="color-font--gray-500">
                        <em>Nov 29 • 7:00 pm</em>
                      </time>
                    </h4>
                  </div>
                </a>
              </li>
              <li class="media media--xs media--right">
                <a class="media__block-link" href="/explore/event/nu-horizons-resume-workshop/">
                  <div class="media__text">
                    <h4 class="media__title">
                      <b>NU Horizons: Resume Workshop</b>
                      <time style="display: block; padding-top: .25rem;" class="color-font--gray-500">
                        <em>Dec 6 • 7:00 pm</em>
                      </time>
                    </h4>
                  </div>
                </a>
              </li>
              <li class="media media--xs media--right">
                <a class="media__block-link" href="/explore/event/nu-horizons-undergraduate-research-panel/">
                  <div class="media__text">
                    <h4 class="media__title">
                      <b>NU Horizons: Undergraduate Research Panel</b>
                      <time style="display: block; padding-top: .25rem;" class="color-font--gray-500">
                        <em>Nov 29 • 7:00 pm</em>
                      </time>
                    </h4>
                  </div>
                </a>
              </li>
              <li class="media media--xs media--right">
                <a class="media__block-link" href="/explore/event/nu-horizons-resume-workshop/">
                  <div class="media__text">
                    <h4 class="media__title">
                      <b>NU Horizons: Resume Workshop</b>
                      <time style="display: block; padding-top: .25rem;" class="color-font--gray-500">
                        <em>Dec 6 • 7:00 pm</em>
                      </time>
                    </h4>
                  </div>
                </a>
              </li>
              <li class="media media--xs media--right">
                <a class="media__block-link" href="/explore/event/nu-horizons-undergraduate-research-panel/">
                  <div class="media__text">
                    <h4 class="media__title">
                      <b>NU Horizons: Undergraduate Research Panel</b>
                      <time style="display: block; padding-top: .25rem;" class="color-font--gray-500">
                        <em>Nov 29 • 7:00 pm</em>
                      </time>
                    </h4>
                  </div>
                </a>
              </li>
              <li class="media media--xs media--right">
                <a class="media__block-link" href="/explore/event/nu-horizons-resume-workshop/">
                  <div class="media__text">
                    <h4 class="media__title">
                      <b>NU Horizons: Resume Workshop</b>
                      <time style="display: block; padding-top: .25rem;" class="color-font--gray-500">
                        <em>Dec 6 • 7:00 pm</em>
                      </time>
                    </h4>
                  </div>
                </a>
              </li>
              <li class="media media--xs media--right">
                <a class="media__block-link" href="/explore/event/nu-horizons-undergraduate-research-panel/">
                  <div class="media__text">
                    <h4 class="media__title">
                      <b>NU Horizons: Undergraduate Research Panel</b>
                      <time style="display: block; padding-top: .25rem;" class="color-font--gray-500">
                        <em>Nov 29 • 7:00 pm</em>
                      </time>
                    </h4>
                  </div>
                </a>
              </li>
              <li class="media media--xs media--right">
                <a class="media__block-link" href="/explore/event/nu-horizons-resume-workshop/">
                  <div class="media__text">
                    <h4 class="media__title">
                      <b>NU Horizons: Resume Workshop</b>
                      <time style="display: block; padding-top: .25rem;" class="color-font--gray-500">
                        <em>Dec 6 • 7:00 pm</em>
                      </time>
                    </h4>
                  </div>
                </a>
              </li>
            </ul>
          </div>
          <footer class="card__footer card__footer--link">
            <a href="#">Full Calendar</a>
          </footer>
        </div>
      </div>


      <div class="col-12@xs col-6@sm col-3@lg">
        <div class="card card--dark">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Basic Card with Image</h4>
              <p class="card__text">This card, with a dark background, has an edge-to-edge image and text in an inner block. Adding the inner block gives padding to the card content.</p>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
          </a>
          <div class="card__badge badge--teal">col 12/6/3</div>
        </div>
      </div>
      <div class="col-12@xs col-6@sm col-12@md col-3@lg">
        <div class="card card--middle card--secondary">
          <a class="card__block-link" href="#">
            <header class="card__header">Card Header</header>
            <div class="card__inner">
              <h4 class="card__title">Basic Card, Centered x&nbsp;&amp;&nbsp;y</h4>
              <p class="card__text">This card's content is centered vertically and horizontally and has a background as the defined secondary color.</p>
            </div>
          </a>
        </div>
      </div>

      <!-- spotlight variations -->
      <div class="col-12@xs col-9@lg">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Spotlight Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
          </a>
          <div class="card__badge badge--teal badge--right">col 12/9 
                <i class="badge__icon ion-upload"></i></div>
        </div>
      </div>
      <div class="col-12@xs col-3@lg">
        <div class="card card--middle card--secondary">
          <a class="card__block-link" href="#">
            <div class="card__inner">
              <h4 class="card__title">Basic Card, Centered x&nbsp;&amp;&nbsp;y</h4>
              <p class="card__text">Curabitur blandit tempus porttitor.</p>
            </div>
          </a>
        </div>
      </div>

      <div class="col-12@xs col-9@lg">
        <div class="card card--dark">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Spotlight Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Etiam porta sem malesuada magna mollis euismod.</p>
              <div class="card__footer">
                <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
                <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
              </div>
            </div>
          </a>
          <div class="card__badge badge--teal">col 12/9</div>
        </div>
      </div>

      <div class="col-12@xs col-6@sm col-4@lg">
        <div class="card card--teal card--bottom">
          <a class="card__block-link" href="#">
            <div class="card__inner">
              <h4 class="card__title">Card with Bottom Text, Primary Background</h4>
              <p class="card__text">Donec ullamcorper nulla non metus auctor fringilla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id elit non mi porta gravida at eget metus. Nulla vitae elit libero, a pharetra augue.</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12@xs col-6@sm col-8@lg">
        <div class="card card--dark">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Spotlight Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Etiam porta sem malesuada magna mollis euismod.</p>
              <div class="card__footer">
                <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
                <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
              </div>
            </div>
          </a>
          <div class="card__badge badge--teal">col 12/6/8</div>
        </div>
      </div>

      <!-- Full width -->
      <div class="col-12@xs">
        <div class="card card--full">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Full-Width Spotlight Card with Image (12)</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
              <div class="card__footer">
                <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
                <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
              </div>
            </div>
          </a>
          <div class="card__badge badge--teal badge--right">col 12</div>
        </div>
      </div>

      <!-- Half 'n' Half at Medium -->
      <div class="col-12@xs col-6@md">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">A Half-Width Spotlight Card with an Image and a Ridiculously Long Headline that Probably Takes Up Three or Four Lines</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Etiam porta sem malesuada magna mollis euismod.</p>
              <div class="card__footer">
                <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
                <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
              </div>
            </div>
          </a>
          <div class="card__badge badge--teal">col 12/6@md</div>
        </div>
      </div>
      <div class="col-12@xs col-6@md">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Half-Width Spotlight Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
              <div class="card__footer">
                <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
                <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
              </div>
            </div>
          </a>
          <div class="card__badge badge--teal badge--right">col 12/6@md</div>
        </div>
      </div>

      <!-- Fifth/Seventh -->
      <div class="col-12@xs col-5@md">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">A Fifth-Width Spotlight Card with an Image and a Ridiculously Long Headline that Probably Takes Up Three or Four Lines</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Etiam porta sem malesuada magna mollis euismod.</p>
              <div class="card__footer">
                <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
                <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
              </div>
            </div>
          </a>
          <div class="card__badge badge--teal">col 12/5@md</div>
        </div>
      </div>
      <div class="col-12@xs col-7@md">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Seventh-Width Spotlight Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
              <div class="card__footer">
                <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
                <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
              </div>
            </div>
          </a>
          <div class="card__badge badge--teal">col 12/7@md</div>
        </div>
      </div>

      <!-- Half 'n' Half at Large -->
      <div class="col-12@xs col-6@lg">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">A Half-Width Spotlight Card with an Image and a Ridiculously Long Headline that Probably Takes Up Three or Four Lines</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Etiam porta sem malesuada magna mollis euismod.</p>
              <div class="card__footer">
                <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
                <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
              </div>
            </div>
          </a>
          <div class="card__badge badge--teal">col 12/6@lg</div>
        </div>
      </div>
      <div class="col-12@xs col-6@lg">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Half-Width Spotlight Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
              <div class="card__footer">
                <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
                <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
              </div>
            </div>
          </a>
          <div class="card__badge badge--teal badge--right">col 12/6@lg</div>
        </div>
      </div>

      <!-- Fifth/Seventh -->
      <div class="col-12@xs col-5@lg">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">A Fifth-Width Spotlight Card with an Image and a Ridiculously Long Headline that Probably Takes Up Three or Four Lines</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Etiam porta sem malesuada magna mollis euismod.</p>
              <div class="card__footer">
                <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
                <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
              </div>
            </div>
          </a>
          <div class="card__badge badge--teal">col 12/5@lg</div>
        </div>
      </div>
      <div class="col-12@xs col-7@lg">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Seventh-Width Spotlight Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
              <div class="card__footer">
                <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
                <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
              </div>
            </div>
          </a>
          <div class="card__badge badge--teal">col 12/7@lg</div>
        </div>
      </div>

      <!-- Fourth/Eighth -->
      <div class="col-12@xs col-4@md">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">A Quarter-Width Spotlight Card with an Image and a Ridiculously Long Headline that Probably Takes Up Three or Four Lines</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Etiam porta sem malesuada magna mollis euismod.</p>
              <div class="card__footer">
                <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
                <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
              </div>
            </div>
          </a>
          <div class="card__badge badge--teal">col 12/4</div>
        </div>
      </div>
      <div class="col-12@xs col-8@md">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Eighth-Width Spotlight Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
              <div class="card__footer">
                <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
                <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
              </div>
            </div>
          </a>
          <div class="card__badge badge--teal">col 12/8</div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="pods-sg-section">
  <header class="pods-sg-section__header">Overlay</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <!-- Quarter/Quarter/Half Overlay -->
      <div class="col-12@xs col-6@sm col-3@lg">
        <div class="card card--overlay">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Overlay Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12@xs col-6@sm col-3@lg">
        <div class="card card--overlay card--bottom card--text-shade">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Overlay Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12@xs col-6@lg">
        <div class="card card--overlay">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Overlay Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
          </a>
        </div>
      </div>

      <!-- Third/Third/Third Overlay -->
      <div class="col-12@xs col-4@md">
        <div class="card card--overlay card--bottom">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">One Third Overlay Card with Image, Text on Bottom</h4>
              <p class="card__text">Nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nascetur ridiculus mus!</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12@xs col-4@md">
        <div class="card card--overlay">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Overlay Card with Image Longtitle</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12@xs col-4@md">
        <div class="card card--overlay">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Overlay Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
          </a>
        </div>
      </div>

      <!-- third/two-thirds overlay -->
      <div class="col-12@xs col-4@md">
        <div class="card card__middle card--overlay">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">One-Third Overlay Card with Image Longtitle</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
            </div>
            <div class="card__footer">
              <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
              <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12@xs col-8@md">
        <div class="card card--overlay">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">Overlay Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
            <div class="card__footer">
              <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
              <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
            </div>
          </a>
        </div>
      </div>

      <!-- seventh/fifth overlay -->
      <div class="col-12@xs col-7@md">
        <div class="card card--overlay">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">One Seventh Overlay Card with Image Longtitle</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
            </div>
            <div class="card__footer">
              <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
              <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12@xs col-5@md">
        <div class="card card--overlay">
          <a class="card__block-link" href="#">
            <div class="card__image__wrapper"></div>
            <div class="card__inner">
              <h4 class="card__title">One Fifth Overlay Card with Image</h4>
              <p class="card__text">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
            <div class="card__footer">
              <div class="content--left"><i class="ion ion-heart"></i> Something Left</div>
              <div class="content--right text--uppercase">Right-o <i class="ion ion-coffee"></i></div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="pods-sg-section">
  <header class="pods-sg-section__header">Large Cards</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-12@xs col-6@sm">
        <div class="card card--lg">
          <a class="card__block-link" href="#">
            <div class="card__inner">
              <h4 class="card__title">Large, Puffy Cards Have Space</h4>
              <p class="card__text">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget metus. Donec id elit non mi porta gravida at eget metus.</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12@xs col-6@sm">
        <div class="card card--lg">
          <a class="card__block-link" href="#">
            <div class="card__inner">
              <h4 class="card__title">Large, Puffy Cards Need Space</h4>
              <p class="card__text">Nullam quis risus eget urna mollis ornare vel eu leo. Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="pods-sg-section dark-bg">
  <header class="pods-sg-section__header">Columns (a la Masonry)</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-12@xs card--columns">
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__inner">
              <h4 class="card__title">1. Basic Card</h4>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
            </div>
          </a>
        </div>
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__inner">
              <h4 class="card__title">2. Basic Card</h4>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
            </div>
          </a>
        </div>
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__inner">
              <h4 class="card__title">3. Basic Card</h4>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
            </div>
          </a>
        </div>
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__inner">
              <h4 class="card__title">4. Basic Card</h4>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
            </div>
          </a>
        </div>
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__inner">
              <h4 class="card__title">5. Basic Card</h4>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
            </div>
          </a>
        </div>
        <div class="card">
          <a class="card__block-link" href="#">
            <div class="card__inner">
              <h4 class="card__title">6. Basic Card</h4>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
              <p class="card__text">Adding the inner block gives padding to the card content.</p>
            </div>
          </a>
        </div>
        </div>
    </div>
  </div>
</section>