<section class="pods-sg-section">
  <div class="banner banner--xs">
    <h1 class="banner__title">Banner XS</h1>
  </div>

  <div class="banner banner--sm">
    <h1 class="banner__title">Banner SM</h1>
  </div>

  <div class="banner">
    <div class="container">
      <div class="row">
        <div class="col-12@sm">
          <div class="banner__pretitle">Banner Pre Title</div>
          <h1 class="banner__title">Banner</h1>
          <div class="banner__subtitle">Banner Sub Title Cras justo odio, dapibus ac facilisis in, egestas eget quam</div>
        </div>
      </div>
    </div>
  </div>

  <div class="banner banner--lg banner--middle">
    <h1 class="banner__title">Banner LG (middle)</h1>
  </div>
</section>

<section class="pods-sg-section">
  <header class="pods-sg-section__header">Image</header>
  <div class="p--y-2@xs">
    <div class="banner banner--image banner--xs">
      <h1 class="banner__title">Banner XS</h1>
    </div>

    <div class="banner banner--image banner--sm">
      <h1 class="banner__title">Banner SM</h1>
    </div>

    <div class="banner banner--image">
      <div class="container">
        <div class="row">
          <div class="col-12@sm">
            <div class="banner__pretitle">Banner Pre Title</div>
            <h1 class="banner__title">Banner Small</h1>
            <div class="banner__subtitle">Banner Sub Title Cras justo odio, dapibus ac facilisis in, egestas eget quam</div>
          </div>
        </div>
      </div>
    </div>

    <div class="banner banner--image banner--lg banner--middle">
      <h1 class="banner__title">Banner LG (middle)</h1>
    </div>
  </div>
</section>

<section class="pods-sg-section">
  <header class="pods-sg-section__header">Transparent</header>
  <div class="p--y-2@xs">
    <div class="banner banner--transparent banner--shadow banner--xs">
      <h1 class="banner__title">Banner XS</h1>
    </div>

    <div class="banner banner--transparent banner--sm">
      <h1 class="banner__title">Banner SM</h1>
    </div>

    <div class="banner banner--transparent banner--shadow">
      <div class="container">
        <div class="row">
          <div class="col-12@sm">
            <div class="banner__pretitle">Banner Pre Title</div>
            <h1 class="banner__title">Banner Small</h1>
            <div class="banner__subtitle">Banner Sub Title Cras justo odio, dapibus ac facilisis in, egestas eget quam</div>
          </div>
        </div>
      </div>
    </div>

    <div class="banner banner--transparent banner--lg banner--middle">
      <h1 class="banner__title">Banner LG (middle)</h1>
    </div>
  </div>
</section>

<section class="pods-sg-section dark-bg">
  <header class="pods-sg-section__header">Transparent Dark</header>
  <div class="p--y-2@xs">
    <div class="banner banner--transparent-dark banner--xs">
      <h1 class="banner__title">Banner XS</h1>
    </div>

    <div class="banner banner--transparent-dark banner--shadow banner--sm">
      <h1 class="banner__title">Banner SM</h1>
    </div>

    <div class="banner banner--transparent-dark">
      <div class="container">
        <div class="row">
          <div class="col-12@sm">
            <div class="banner__pretitle">Banner Pre Title</div>
            <h1 class="banner__title">Banner Small</h1>
            <div class="banner__subtitle">Banner Sub Title Cras justo odio, dapibus ac facilisis in, egestas eget quam</div>
          </div>
        </div>
      </div>
    </div>

    <div class="banner banner--transparent-dark banner--shadow banner--lg banner--middle">
      <h1 class="banner__title">Banner LG (middle)</h1>
    </div>
  </div>
</section>

<section class="pods-sg-section subtle-bg">
  <header class="pods-sg-section__header">White</header>
  <div class="p--y-2@xs">
    <div class="banner banner--white banner--shadow banner--xs">
      <h1 class="banner__title">Banner XS</h1>
    </div>

    <div class="banner banner--white banner--sm">
      <h1 class="banner__title">Banner SM</h1>
    </div>

    <div class="banner banner--white banner--shadow">
      <div class="container">
        <div class="row">
          <div class="col-12@sm">
            <div class="banner__pretitle">Banner Pre Title</div>
            <h1 class="banner__title">Banner Small</h1>
            <div class="banner__subtitle">Banner Sub Title Cras justo odio, dapibus ac facilisis in, egestas eget quam</div>
          </div>
        </div>
      </div>
    </div>

    <div class="banner banner--white banner--lg banner--middle">
      <h1 class="banner__title">Banner LG (middle)</h1>
    </div>
  </div>
</section>
