<section class="pods-sg-section">
  <header class="pods-sg-section__header">Base</header>
  <div class="sg-wrapper">
    <!-- start base masthead -->
    <header class="masthead">
      <a class="masthead__logo" href="/">
      <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo.png">
      </a>
      <button class="masthead__toggler hidden--up@lg ion-navicon"></button>
      <nav class="masthead__drawer" role="navigation">
        <div class="clearfix hidden--up@lg">
          <a data-ga-click="" class="masthead__logo" href="/">
          <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo-white.png">
          </a>
          <button class="masthead__toggler menu-is-open ion-close"></button>
        </div>
        <ul class="masthead__menu__list">
          <li class="menu-item active">
            <a class="menu-link" href="https://www.google.com/">Lorem Ipsum</a>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Bibendum</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Quam Vulputate Nibh</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Tortor Fusce</a></li>
            </ul>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Pudgi Homunculi</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Malesuada Bibendum</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Bibendum Mattis Dapibus</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Venenatis Pharetra Sit Dolor</a></li>
            </ul>
          </li>
          <li class="menu-item">
            <a class="menu-link" href="https://www.google.com/">Malesuada Nucleus</a>
          </li>
        </ul>
      </nav>
    </header>
    <!-- end base masthead -->
    <div class="container-fluid">
      <div class="row">
        <div class="banner banner--image banner--middle">
          <p class="banner__title p--t-4@xs">{ <i>Example Hero</i> }</p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Full-width masthead -->
<section class="pods-sg-section">
  <header class="pods-sg-section__header">Full-Width</header>
  <div class="sg-wrapper">

    <!-- start full-width masthead -->
    <header class="masthead masthead--wide">
      <a class="masthead__logo" href="/">
      <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo.png">
      </a>
      <button class="masthead__toggler hidden--up@lg ion-navicon"></button>
      <nav class="masthead__drawer" role="navigation">
        <div class="clearfix hidden--up@lg">
          <a data-ga-click="" class="masthead__logo" href="/">
          <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo-white.png">
          </a>
          <button class="masthead__toggler menu-is-open ion-close"></button>
        </div>

        <ul class="masthead__menu__list">
          <li class="menu-item active">
            <a class="menu-link" href="https://www.google.com/">Lorem Ipsum</a>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Bibendum</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Quam Vulputate Nibh</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Tortor Fusce</a></li>
            </ul>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Pudgi Homunculi</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Malesuada Bibendum</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Bibendum Mattis Dapibus</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Venenatis Pharetra Sit Dolor</a></li>
            </ul>
          </li>
          <li class="menu-item">
            <a class="menu-link" href="https://www.google.com/">Malesuada Nucleus</a>
          </li>
        </ul>
      </nav>
    </header>
    <!-- end full-width masthead -->
    <div class="container-fluid">
      <div class="row">
        <div class="banner banner--image banner--middle">
          <p class="banner__title p--t-4@xs">{ <i>Example Hero</i> }</p>
        </div>
      </div>
    </div>

  </div>
</section>

<!-- Image background overlay masthead -->
<section class="pods-sg-section">
  <header class="pods-sg-section__header">Image Background Overlay</header>
  <div class="sg-wrapper">
    <!-- start background-image-overlay masthead -->
    <header class="masthead masthead--overlay m--t-2@xs">
      <a class="masthead__logo" href="/">
      <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo-white.png">
      </a>
      <button class="masthead__toggler hidden--up@lg ion-navicon"></button>
      <nav class="masthead__drawer" role="navigation">
        <div class="clearfix hidden--up@lg">
          <a data-ga-click="" class="masthead__logo" href="/">
          <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo-white.png">
          </a>
          <button class="masthead__toggler menu-is-open ion-close"></button>
        </div>

        <ul class="masthead__menu__list">
          <li class="menu-item active">
            <a class="menu-link" href="https://www.google.com/">Lorem Ipsum</a>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Bibendum</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Quam Vulputate Nibh</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Tortor Fusce</a></li>
            </ul>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Pudgi Homunculi</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Malesuada Bibendum</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Bibendum Mattis Dapibus</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Venenatis Pharetra Sit Dolor</a></li>
            </ul>
          </li>
          <li class="menu-item">
            <a class="menu-link" href="https://www.google.com/">Malesuada Nucleus</a>
          </li>
        </ul>
      </nav>
    </header>
    <!-- end background-image-overlay masthead -->
    <div class="container-fluid">
      <div class="row">
        <div class="banner banner--image banner--lg
        ">
          <p class="banner__title ta--c p--t-2@xs">{ <i>Example Hero</i> }</p>
        </div>
      </div>
    </div>

  </div>
</section>

<!-- Image full-width black-background masthead -->
<section class="pods-sg-section">
  <header class="pods-sg-section__header">Full-Width Black-Background Masthead with Image Overlay Hero</header>
  <div class="sg-wrapper">
    <!-- start full-width background-image-overlay masthead -->
    <header class="masthead masthead--black masthead--wide m--t-2@xs">
      <a class="masthead__logo" href="/">
      <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo-white.png">
      </a>
      <button class="masthead__toggler hidden--up@lg ion-navicon"></button>
      <nav class="masthead__drawer" role="navigation">
        <div class="clearfix hidden--up@lg">
          <a data-ga-click="" class="masthead__logo" href="/">
          <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo-white.png">
          </a>
          <button class="masthead__toggler menu-is-open ion-close"></button>
        </div>

        <ul class="masthead__menu__list">
          <li class="menu-item active">
            <a class="menu-link" href="https://www.google.com/">Lorem Ipsum</a>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Bibendum</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Quam Vulputate Nibh</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Tortor Fusce</a></li>
            </ul>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Pudgi Homunculi</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Malesuada Bibendum</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Bibendum Mattis Dapibus</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Venenatis Pharetra Sit Dolor</a></li>
            </ul>
          </li>
          <li class="menu-item">
            <a class="menu-link" href="https://www.google.com/">Malesuada Nucleus</a>
          </li>
        </ul>
      </nav>
    </header>
    <!-- end full-width background-image-overlay masthead -->
    <div class="container-fluid">
      <div class="row">
        <div class="banner banner--image banner--lg">
          <p class="banner__title ta--c p--t-2@xs">{ <i>Example Hero</i> }</p>
        </div>
      </div>
    </div>

  </div>
</section>

<!-- Image full-width background overlay masthead -->
<section class="pods-sg-section">
  <header class="pods-sg-section__header">Full-Width Image Background Overlay</header>
  <div class="sg-wrapper">
    <!-- start full-width background-image-overlay masthead -->
    <header class="masthead masthead--overlay masthead--wide m--t-2@xs">
      <a class="masthead__logo" href="/">
      <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo-white.png">
      </a>
      <button class="masthead__toggler hidden--up@lg ion-navicon"></button>
      <nav class="masthead__drawer" role="navigation">
        <div class="clearfix hidden--up@lg">
          <a data-ga-click="" class="masthead__logo" href="/">
          <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo-white.png">
          </a>
          <button class="masthead__toggler menu-is-open ion-close"></button>
        </div>

        <ul class="masthead__menu__list">
          <li class="menu-item active">
            <a class="menu-link" href="https://www.google.com/">Lorem Ipsum</a>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Bibendum</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Quam Vulputate Nibh</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Tortor Fusce</a></li>
            </ul>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Pudgi Homunculi</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Malesuada Bibendum</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Bibendum Mattis Dapibus</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Venenatis Pharetra Sit Dolor</a></li>
            </ul>
          </li>
          <li class="menu-item">
            <a class="menu-link" href="https://www.google.com/">Malesuada Nucleus</a>
          </li>
        </ul>
      </nav>
    </header>
    <!-- end full-width background-image-overlay masthead -->
    <div class="container-fluid">
      <div class="row">
        <div class="banner banner--image banner--lg">
          <p class="banner__title ta--c p--t-2@xs">{ <i>Example Hero</i> }</p>
        </div>
      </div>
    </div>

  </div>
</section>

<section class="pods-sg-section">
  <header class="pods-sg-section__header">Bottom-Bordered Selection</header>
  <div class="sg-wrapper">
    <!-- start base masthead -->
    <header class="masthead">
      <a class="masthead__logo" href="/">
      <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo.png">
      </a>
      <button class="masthead__toggler hidden--up@lg ion-navicon"></button>
      <nav class="masthead__drawer" role="navigation">
        <div class="clearfix hidden--up@lg">
          <a data-ga-click="" class="masthead__logo" href="/">
          <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo-white.png">
          </a>
          <button class="masthead__toggler menu-is-open ion-close"></button>
        </div>
        <ul class="masthead__menu__list">
          <li class="menu-item active">
            <a class="menu-link" href="https://www.google.com/">Lorem Ipsum</a>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Bibendum</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Quam Vulputate Nibh</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Tortor Fusce</a></li>
            </ul>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Pudgi Homunculi</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Malesuada Bibendum</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Bibendum Mattis Dapibus</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Venenatis Pharetra Sit Dolor</a></li>
            </ul>
          </li>
          <li class="menu-item">
            <a class="menu-link" href="https://www.google.com/">Malesuada Nucleus</a>
          </li>
        </ul>
      </nav>
    </header>
    <!-- end border-bottom masthead -->
  </div>
</section>

<!-- Full-width masthead -->
<section class="pods-sg-section">
  <header class="pods-sg-section__header">Full-Width Bottom-Bordered Selection</header>
  <div class="sg-wrapper">

    <!-- start full-width masthead -->
    <header class="masthead masthead--wide">
      <a class="masthead__logo" href="/">
      <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo.png">
      </a>
      <button class="masthead__toggler hidden--up@lg ion-navicon"></button>
      <nav class="masthead__drawer" role="navigation">
        <div class="clearfix hidden--up@lg">
          <a data-ga-click="" class="masthead__logo" href="/">
          <img class="masthead__logo__image" alt="Logo" src="/dist/images/logo-white.png">
          </a>
          <button class="masthead__toggler menu-is-open ion-close"></button>
        </div>

        <ul class="masthead__menu__list">
          <li class="menu-item active">
            <a class="menu-link" href="https://www.google.com/">Lorem Ipsum</a>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Bibendum</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Quam Vulputate Nibh</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Tortor Fusce</a></li>
            </ul>
          </li>
          <li class="menu-item menu-item-has-children">
            <a class="menu-link" href="https://www.google.com/">Pudgi Homunculi</a>
            <ul class="sub-menu">
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Malesuada Bibendum</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Bibendum Mattis Dapibus</a></li>
              <li class="menu-item"><a class="menu-link" href="https://www.google.com/">Venenatis Pharetra Sit Dolor</a></li>
            </ul>
          </li>
          <li class="menu-item">
            <a class="menu-link" href="https://www.google.com/">Malesuada Nucleus</a>
          </li>
        </ul>
      </nav>
    </header>
    <!-- end full-width border-bottom masthead -->

  </div>
</section>
