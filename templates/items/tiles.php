<section class="pods-sg-section subtle-bg">
  <header class="pods-sg-section__header">Base</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile">
          <a class="tile__block-link" href="#">
            <h4 class="tile__title">Basic Tile</h4>
            <p class="tile__text">This is the tile in its most basic form.</p>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile">
          <a class="tile__block-link" href="#">
            <div class="tile__inner">
              <h4 class="tile__title">Basic Tile</h4>
              <p class="tile__text">This is the tile in its most basic form, but with padding.</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile tile--middle tile--outline">
          <a class="tile__block-link" href="#">
            <div class="tile__inner">
              <h4 class="tile__title">Basic Tile</h4>
              <p class="tile__text">This is a tile with centered content.</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile tile--border-bottom">
          <a class="tile__block-link" href="#">
            <header class="tile__header">Tile Header<i class="ion-ios-arrow-thin-right"></i></header>
            <div class="tile__inner">
              <h4 class="tile__title">Basic Tile</h4>
              <p class="tile__text">This is a tile with a bottom border.</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile tile--middle">
          <a class="tile__block-link" href="#">
            <div class="tile__inner">
              <div class="tile__image__wrapper">
                <img class="tile__image" src="//vignette3.wikia.nocookie.net/muppet/images/3/3c/CT-p0001-ST.jpg/revision/latest?cb=20060205225316" alt="">
              </div>
              <h4 class="tile__title">This tile is centered with an image!</h4>
            </div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile tile--overlay">
          <a class="tile__block-link" href="#">
            <div class="tile__inner">
              <h4 class="tile__title">This tile has a solid overlay</h4>
            </div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile tile--overlay--image">
          <a class="tile__block-link" href="#">
          <div class="tile__image__wrapper">
            <img class="tile__image" src="//farm2.staticflickr.com/1628/23687981893_21cab0f191_z_d.jpg" alt="">
          </div>
            <div class="tile__inner">
              <div class="align-bottom">
                <h4 class="tile__title">Image Tile</h4>
                <p class="tile__text">This tile has a background image.</p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile tile--outline">
          <a class="tile__block-link" href="#">
            <div class="tile__inner">
              <h4 class="tile__title">Basic Tile, Longish Title, CTA<i class="ion-locked"></i></h4>
              <p class="tile__text">This is a tile with an outline and title CTA.</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile tile--middle tile--beige-light">
          <a class="tile__block-link" href="#">
            <div class="tile__inner">
              <h4 class="tile__title">Basic Tile, Primary</h4>
              <p class="tile__text">This is a tile with centered content.</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile tile--border-bottom tile--teal tile--bottom">
          <a class="tile__block-link" href="#">
            <div class="tile__inner">
              <h4 class="tile__title">Basic Tile, Secondary Border</h4>
              <p class="tile__text">This is a tile with a bottom border.</p>
            </div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile tile--middle tile--tertiary">
          <a class="tile__block-link" href="#">
            <div class="tile__inner">
              <div class="tile__image__wrapper">
                <img class="tile__image" src="//s-media-cache-ak0.pinimg.com/236x/c3/ca/67/c3ca67481cd8019f8aa863d83300f2a5.jpg" alt="">
              </div>
              <h4 class="tile__title">This tile is centered with an image!</h4>
            </div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile tile--overlay">
          <a class="tile__block-link" href="#">
            <div class="tile__inner">
              <h4 class="tile__title">This tile has a solid overlay</h4>
            </div>
          </a>
        </div>
      </div>

      <!-- stinkin' badges -->

      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile">
          <a class="tile__block-link" href="#">
            <div class="tile__inner p--t-3@xs">
              <h4 class="tile__title">Tile With Badge<i class="ion-ios-arrow-thin-right"></i></h4>
              <p class="tile__text">This is the tile in its most basic form, but with padding.</p>
            </div>
            <div class="tile__badge">Tile Badge</div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile">
          <a class="tile__block-link" href="#">
            <div class="tile__inner p--t-3@xs">
              <h4 class="tile__title">Tile With Badge</h4>
              <p class="tile__text">This is the tile in its most basic form, but with padding.</p>
            </div>
            <div class="tile__badge">Tile Badge</div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile">
          <a class="tile__block-link" href="#">
            <div class="tile__inner p--t-3@xs">
              <h4 class="tile__title">Tile With Badge</h4>
              <p class="tile__text">This is the tile in its most basic form, but with padding.</p>
            </div>
            <div class="tile__badge badge--center">Badge Center</div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile">
          <a class="tile__block-link" href="#">
            <div class="tile__inner p--t-3@xs">
              <h4 class="tile__title">Tile With Badge</h4>
              <p class="tile__text">This is the tile in its most basic form, but with padding.</p>
            </div>
            <div class="tile__badge badge--center">Badge Center</div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile">
          <a class="tile__block-link" href="#">
            <div class="tile__inner p--t-3@xs">
              <h4 class="tile__title">Tile With Badge</h4>
              <p class="tile__text">This is the tile in its most basic form, but with padding.</p>
            </div>
            <div class="tile__badge badge--right">Badge Right</div>
          </a>
        </div>
      </div>
      <div class="col-6@xs col-4@sm col-3@md col-2@lg">
        <div class="tile">
          <a class="tile__block-link" href="#">
            <div class="tile__inner p--t-3@xs">
              <h4 class="tile__title">Tile With Badge</h4>
              <p class="tile__text">This is the tile in its most basic form, but with padding.</p>
            </div>
            <div class="tile__badge badge--right">Badge Right</div>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
