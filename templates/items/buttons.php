<section class="pods-sg-section">
  <header class="pods-sg-section__header">Base</header>
  <div class="container ta--c">
      <div class="row">
        <div class="col-12@xs col-4@md p--y-2@xs">
          <p><a class="btn btn--xs" data-component="button" href="#">Button xs</a></p>
          <p><a class="btn btn--pill btn--sm" data-component="button" href="#">Button Pill sm</a></p>
          <p><a class="btn" data-component="button" href="#">Button</a></p>
          <p><a class="btn btn--lg" data-component="button" href="#">Button lg</a></p>
          <p><a class="btn btn--block btn--lg" data-component="button" href="#">Button block lg</a></p>
        </div>
        <div class="col-12@xs col-4@md p--y-2@xs">
          <p><a class="btn btn--pill btn--xs btn--red" data-component="button" href="#">Red Pill xs</a></p>
          <p><a class="btn btn--sm btn--red btn--outline" data-component="button" href="#">Red Outline sm</a></p>
          <p><a class="btn btn--red" data-component="button" href="#">Red</a></p>
          <p><a class="btn btn--lg btn--red btn--outline" data-component="button" href="#">Red Outline lg</a></p>
          <p><a class="btn btn--block btn--lg btn--red" data-component="button" href="#">Block Red lg</a></p>
        </div>
        <div class="col-12@xs col-4@md p--y-2@xs">
          <p><a class="btn btn--outline btn--xs btn--gray-dark" data-component="button" href="#">Outline xs</a></p>
          <p><a class="btn btn--outline btn--sm btn--gray-dark" data-component="button" href="#">Outline sm</a></p>
          <p><a class="btn btn--pill btn--outline btn--gray-dark" data-component="button" href="#">Pill Outline</a></p>
          <p><a class="btn btn--outline btn--lg btn--gray-dark" data-component="button" href="#">Outline lg</a></p>
          <p><a class="btn btn--outline btn--block btn--lg btn--gray-dark" data-component="button" href="#">Block Outline lg</a></p>
        </div>
      </div>
  </div>
</section>
<section class="pods-sg-section dark-bg">
  <header class="pods-sg-section__header">Base</header>
  <div class="container ta--c">
      <div class="row">
        <div class="col-12@xs col-4@md p--y-2@xs">
          <p><a class="btn btn--xs" data-component="button" href="#">Button xs</a></p>
          <p><a class="btn btn--pill btn--sm" data-component="button" href="#">Button Pill sm</a></p>
          <p><a class="btn" data-component="button" href="#">Button</a></p>
          <p><a class="btn btn--lg" data-component="button" href="#">Button lg</a></p>
          <p><a class="btn btn--block btn--lg" data-component="button" href="#">Button block lg</a></p>
        </div>
        <div class="col-12@xs col-4@md p--y-2@xs">
          <p><a class="btn btn--pill btn--xs btn--teal" data-component="button" href="#">Teal Pill xs</a></p>
          <p><a class="btn btn--sm btn--teal-light btn--outline" data-component="button" href="#">Light Teal Outline sm</a></p>
          <p><a class="btn btn--teal" data-component="button" href="#">Teal</a></p>
          <p><a class="btn btn--lg btn--teal-light btn--outline" data-component="button" href="#">Light Teal Outline lg</a></p>
          <p><a class="btn btn--block btn--lg btn--teal" data-component="button" href="#">Block Teal lg</a></p>
        </div>
        <div class="col-12@xs col-4@md p--y-2@xs">
          <p><a class="btn btn--outline btn--white btn--xs" data-component="button" href="#">White Outline xs</a></p>
          <p><a class="btn btn--outline btn--white btn--sm" data-component="button" href="#">White Outline sm</a></p>
          <p><a class="btn btn--pill btn--white btn--outline" data-component="button" href="#">Pill White Outline</a></p>
          <p><a class="btn btn--outline btn--white btn--lg" data-component="button" href="#">White Outline lg</a></p>
          <p><a class="btn btn--outline btn--white btn--block btn--lg" data-component="button" href="#">Block White Outline lg</a></p>
        </div>
      </div>
  </div>
</section>
