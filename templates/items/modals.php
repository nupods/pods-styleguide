<section class="pods-sg-section">
  <header class="pods-sg-section__header">Base</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-12@xs col-4@md offset-4@md">
        <p><a class="btn btn--outline btn--gray btn--sm" href="#" data-toggle="modal" data-target="#modal_base">Modal Me</a></p>
        <div class="modal fade" id="modal_base" tabindex="-1" role="dialog" aria-labelledby="modal_base_label" aria-hidden="true">
          <div class="modal-dialog modal__dialog" role="document">
            <div class="modal__content">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="modal__header">
                <h4 class="modal__title" id="modal_base_label">Modal-lay-hee-hoo!</h4>
              </div>
              <div class="modal__body">
                <p>Cras mattis consectetur purus sit amet fermentum. Maecenas faucibus mollis interdum. Maecenas faucibus mollis interdum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus.</p>
              </div>
              <div class="modal__footer">
                <button type="button" class="btn btn--teal btn--sm" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn--sm">Save changes</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="pods-sg-section">
  <header class="pods-sg-section__header">Dark</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-12@xs col-4@md offset-4@md">
        <p><a class="btn btn--outline btn--gray btn--sm" href="#" data-toggle="modal" data-target="#modal_dark">Modal Me</a></p>
        <div class="modal modal--dark fade" id="modal_dark" tabindex="-1" role="dialog" aria-labelledby="modal_dark_label" aria-hidden="true">
          <div class="modal-dialog modal__dialog" role="document">
            <div class="modal__content">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="modal__header">
                <h4 class="modal__title" id="modal_base_label">Modal-lay-hee-hoo!</h4>
              </div>
              <div class="modal__body">
                <p>Cras mattis consectetur purus sit amet fermentum. Maecenas faucibus mollis interdum. Maecenas faucibus mollis interdum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus.</p>
              </div>
              <div class="modal__footer">
                <button type="button" class="btn btn--teal btn--sm" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn--sm">Save changes</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="pods-sg-section">
  <header class="pods-sg-section__header">Contact</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-12@xs col-4@md offset-4@md">
        <p><a class="btn btn--outline btn--gray btn--sm" href="#" data-toggle="modal" data-target="#modal_contact">Contact Me</a></p>
        <div class="modal modal--contact fade" id="modal_contact" tabindex="-1" role="dialog" aria-labelledby="modal_contact_label" aria-hidden="true">
          <div class="modal-dialog modal__dialog" role="document">
            <div class="modal__content">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="row">
                <div class="col-12@xs col-4@md">
                  <div class="modal__image__wrapper">
                    <img class="modal__image" src="//farm2.staticflickr.com/1672/23697640273_90b5c92b84_z_d.jpg" alt="">
                  </div>
                  <div class="modal__body">
                    <p class="modal--contact__info">vfleming@northeastern.edu<br>
                    617.373.5555</p>
                  </div>
                </div>
                <div class="col-12@xs col-8@md">
                  <div class="modal__body modal--contact__bio">
                    <h3 class="modal--contact__name">David Mary Davenport</h3>
                    <p class="modal--contact__title">Curabitur Blandit Tempus Porttitor<br>
                    Egestas Eget Quam, Venenatis</p>
                    <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Donec ullamcorper nulla non metus auctor fringilla. Curabitur blandit tempus porttitor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Donec ullamcorper nulla non metus auctor fringilla. Curabitur blandit tempus porttitor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
