<section class="pods-sg-section">
  <div class="container p--y-2@xs">
    <!-- Black -->
    <div class="row color-cell">
      <div class="col-12@xs bg-black"><span class="hex-color">#000000</span> Black</div>
    </div>
    <!-- Grays -->
    <div class="row color-cell">
      <div class="col-12@xs col-4@sm col-3@lg bg-gray--dark"><span class="hex-color">#1a1a1a</span> Dark Gray <span class="fw--n">*</span></div>
      <div class="col-12@xs col-4@sm col-6@lg bg-gray"><span class="hex-color">#58585a</span> Gray <i class="fw--n">(Dark Gray)</i></div>
      <div class="col-12@xs col-4@sm col-3@lg bg-gray--light"><span class="hex-color">#7b7778</span> Light Gray</div>
    </div>
    <!-- Reds -->
    <div class="row color-cell">
      <div class="col-12@xs col-4@sm col-3@lg bg-red--dark"><span class="hex-color">#930000</span> Dark Red <span class="fw--n">*</span></div>
      <div class="col-12@xs col-4@sm col-6@lg bg-red"><span class="hex-color">#cc0000</span> Red</div>
      <div class="col-12@xs col-4@sm col-3@lg bg-red--light"><span class="hex-color">#da3426</span> Light Red <span class="fw--n">*</span></div>
    </div>
    <!-- Blues -->
    <div class="row color-cell">
      <div class="col-12@xs col-4@sm col-3@lg bg-blue--dark"><span class="hex-color">#2f4a61</span> Dark Blue</div>
      <div class="col-12@xs col-4@sm col-6@lg bg-blue"><span class="hex-color">#5c7d9e</span> Blue <span class="fw--n">*</span></div>
      <div class="col-12@xs col-4@sm col-3@lg bg-blue--light"><span class="hex-color">#aec2da</span> Light Blue</div>
    </div>
    <!-- Greens -->
    <div class="row color-cell">
      <div class="col-12@xs col-4@sm col-3@lg bg-green--dark"><span class="hex-color">#808c15</span> Dark Green <span class="fw--n">*</span></div>
      <div class="col-12@xs col-4@sm col-6@lg bg-green"><span class="hex-color">#aebe42</span> Green</div>
      <div class="col-12@xs col-4@sm col-3@lg bg-green--light"><span class="hex-color">#d4e291</span> Light Green</div>
    </div>
    <!-- Purples -->
    <div class="row color-cell">
      <div class="col-12@xs col-4@sm col-3@lg bg-purple--dark"><span class="hex-color">#511951</span> Dark Purple <i class="fw--n">(Purple)</i></div>
      <div class="col-12@xs col-4@sm col-6@lg bg-purple"><span class="hex-color">#9d64a2</span> Purple <span class="fw--n">*</span></div>
      <div class="col-12@xs col-4@sm col-3@lg bg-purple--light"><span class="hex-color">#d0b7d5</span> Light Purple <span class="fw--n">*</span></div>
    </div>
    <!-- Yellows -->
    <div class="row color-cell">
      <div class="col-12@xs col-4@sm col-3@lg bg-yellow--dark"><span class="hex-color">#f9b301</span> Dark Yellow <span class="fw--n">*</span></div>
      <div class="col-12@xs col-4@sm col-6@lg bg-yellow"><span class="hex-color">#fec42f</span> Yellow</div>
      <div class="col-12@xs col-4@sm col-3@lg bg-yellow--light"><span class="hex-color">#fed262</span> Light Yellow <span class="fw--n">*</span></div>
    </div>
    <!-- Oranges -->
    <div class="row color-cell">
      <div class="col-12@xs col-4@sm col-3@lg bg-orange--dark"><span class="hex-color">#d15f08</span> Dark Orange <span class="fw--n">*</span></div>
      <div class="col-12@xs col-4@sm col-6@lg bg-orange"><span class="hex-color">#f68c1e</span> Orange</div>
      <div class="col-12@xs col-4@sm col-3@lg bg-orange--light"><span class="hex-color">#f9b268</span> Light Orange <span class="fw--n">*</span></div>
    </div>
    <!-- Teals -->
    <div class="row color-cell">
      <div class="col-12@xs col-4@sm col-3@lg bg-teal--dark"><span class="hex-color">#13606d</span> Dark Teal <span class="fw--n">*</span></div>
      <div class="col-12@xs col-4@sm col-6@lg bg-teal"><span class="hex-color">#309aa8</span> Teal <i class="fw--n">(Teal Blue)</i></div>
      <div class="col-12@xs col-4@sm col-3@lg bg-teal--light"><span class="hex-color">#aed8da</span> Light Teal</div>
    </div>
    <!-- Creams -->
    <div class="row color-cell">
      <div class="col-12@xs col-4@sm col-3@lg bg-beige--dark"><span class="hex-color">#615547</span> Dark Beige <span class="fw--n">*</span></div>
      <div class="col-12@xs col-4@sm col-6@lg bg-beige"><span class="hex-color">#bdb29f</span> Beige <span class="fw--n">*</span></div>
      <div class="col-12@xs col-4@sm col-3@lg bg-beige--light"><span class="hex-color">#ece6d5</span> Beige <i class="fw--n">(Cream)</i></div>
    </div>
    <!-- Neutral Grays -->
    <div class="row color-cell">
      <div class="col-4@xs col-1@lg bg-gray--900"><span class="hex-color">#1a1a1a</span><br>Gray 900</div>
      <div class="col-4@xs col-1@lg bg-gray--800"><span class="hex-color">#333333</span><br>Gray 800</div>
      <div class="col-4@xs col-1@lg bg-gray--700"><span class="hex-color">#535353</span><br>Gray 700</div>
      <div class="col-4@xs col-1@lg bg-gray--600"><span class="hex-color">#666666</span><br>Gray 600</div>
      <div class="col-4@xs col-1@lg bg-gray--500"><span class="hex-color">#797979</span><br>Gray 500</div>
      <div class="col-4@xs col-1@lg bg-gray--400"><span class="hex-color">#969696</span><br>Gray 400</div>
      <div class="col-4@xs col-1@lg bg-gray--300"><span class="hex-color">#b5b5b5</span><br>Gray 300</div>
      <div class="col-4@xs col-1@lg bg-gray--200"><span class="hex-color">#d0d0d0</span><br>Gray 200</div>
      <div class="col-4@xs col-1@lg bg-gray--100"><span class="hex-color">#efefef</span><br>Gray 100</div>
    </div>

    <div class="row">
      <div class="col-12@xs">
        <p class="fs--xs">* Created by PODS to augment the 2017 NU brand colors.</p>
      </div>
    </div>
  </div>
</section>