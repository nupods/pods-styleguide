<section class="pods-sg-section">
  <header class="pods-sg-section__header">Base</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-4@lg">
        <blockquote class="blockquote">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
        </blockquote>
      </div>
      <div class="col-4@lg">
        <blockquote class="blockquote blockquote--mark">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
        </blockquote>
      </div>
      <div class="col-4@lg">
        <blockquote class="blockquote blockquote--mark blockquote--purple-dark">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
          <div class="blockquote__cite">Person Name</div>
          <div class="blockquote__source">Washington Post</div>
        </blockquote>
      </div>
    </div>
  </div>
</section>

<section class="pods-sg-section">
  <header class="pods-sg-section__header">White</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-4@lg">
        <blockquote class="blockquote blockquote--white shadow--none">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
        </blockquote>
      </div>
      <div class="col-4@lg">
        <blockquote class="blockquote blockquote--white blockquote--mark shadow--none">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
        </blockquote>
      </div>
      <div class="col-4@lg">
        <blockquote class="blockquote blockquote--white blockquote--mark">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
          <div class="blockquote__cite">Person Name</div>
          <div class="blockquote__source">Washington Post</div>
        </blockquote>
      </div>
    </div>
  </div>
</section>

<section class="pods-sg-section">
  <header class="pods-sg-section__header">Outline</header>
  <div class="container p--y-2@xs">
    <div class="row">
      <div class="col-4@lg">
        <blockquote class="blockquote blockquote--outline">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
        </blockquote>
      </div>
      <div class="col-4@lg">
        <blockquote class="blockquote blockquote--outline blockquote--mark blockquote--teal">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
        </blockquote>
      </div>
      <div class="col-4@lg">
        <blockquote class="blockquote blockquote--outline blockquote--mark blockquote--orange-dark">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
          <div class="blockquote__cite">Person Name</div>
          <div class="blockquote__source">Washington Post</div>
        </blockquote>
      </div>
    </div>
  </div>
</section>
