<?php
/**
 * Get the style guide files
 * @return array list of files
 */
function getStyleGuideFiles() {
  $dir = new DirectoryIterator(dirname(__FILE__).'/templates/items');
  $files = [];
  foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot()) {
      $files[] = preg_replace("/(.+)\.php$/", "$1", $fileinfo->getFilename());
    }
  }
  return $files;
}

/**
 * Get bower.json data
 * @return version value
 */

function getBowerData() {
  $str = file_get_contents('./bower.json', FILE_USE_INCLUDE_PATH);
  $json = json_decode($str, true); // decode the JSON into an associative array
  $version = $json['version'];
  return $version;
}

?>
